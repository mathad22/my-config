# Manjaro shell commands :

```bash
	sudo ln -sf ~/my-config/manjaro/.fonts ~/.
	sudo ln -sf ~/my-config/manjaro/i3/.config ~/.i3/.config
	sudo ln -sf ~/my-config/manjaro/rofi/.Xresources ~/.
```
